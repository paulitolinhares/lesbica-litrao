import React, { Component } from 'react';
import './App.css';
import getRandomExcuse from 'get-random-excuse';
import seedData from './data/data.json';
import cover from './cover.jpg'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tweet: getRandomExcuse(seedData),
    };
  }
  newTweet = () => {
    this.setState({
      tweet: getRandomExcuse(seedData)
    })
  }

  tweet = () => {
    const text = `${this.state.tweet}, #LesbicaLitrao https://lesbica-litrao.herokuapp.com/`
    const paramText = encodeURIComponent(text);
    const url = `https://twitter.com/intent/tweet?&text=${paramText}&wrap_links=true`;
    window.open(
      url,
      '', 
      'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'
    );
    return false;
  }

  render() {
    const { tweet } = this.state;
    return (
      <div className="App">
        <img src={cover} />
        <p>{ tweet }</p>
        <div className="App-buttons">
          <button onClick={this.newTweet}>Gerar outro</button>
          <button onClick={this.tweet}>Compartilhar no twitter</button>
        </div>
      </div>
    );
  }
}

export default App;
